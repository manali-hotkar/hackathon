package amazon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class RoadConnectivity {
	
	/******
	 * City contains bad roads and not all cities are connected directly or indirectly.
	 * Bob is at point A and wants to know if he can reach point B
	 * I/p:	First line contains n number of points and m number of roads
	 * Next m lines contains u point and v point connected directly (bidirectional road)
	 * Next line contains point x and point y to check if connected. if connected print 1 else 0
	 * e.g 
	 * i/p
	 * 3 2
	 * 1 2
	 * 2 3
	 * 1 3
	 * o/p -> 1 
	 *****/

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no of points");
		int nPoints = sc.nextInt();
		//System.out.println("Enter no of connected roads");
		int mRoads = sc.nextInt();
		
		Set<Integer> roadList = new HashSet<Integer>();
		Map<Integer, ArrayList<Integer>> mapRoads = new HashMap<Integer, ArrayList<Integer>>();
		int index=0;
		//int[][] connectedRoads = new int[mRoads] [];
		
		for(int i=0; i < mRoads ; i++) {
			System.out.println("Enter connected points for road : "+ i);
			int uPoint = sc.nextInt();
			//System.out.println("Enter 2nd point for road "+ i);
			int vPoint = sc.nextInt();
			
			if(mapRoads.isEmpty()) {
				roadList.add(uPoint);
				roadList.add(vPoint);
				continue;
			}
			
			if(roadList.contains(uPoint))
				roadList.add(vPoint);
			else if(roadList.contains(vPoint))
				roadList.add(uPoint);
			else {
				ArrayList<Integer> newRoadList = new ArrayList<Integer>();
				newRoadList.add(uPoint);
				newRoadList.add(vPoint);
				mapRoads.put(index, newRoadList);
				index++;
			}
		}
		
		for(int i=0; i < index; i++) {
			ArrayList<Integer> newRoadList = mapRoads.get(i);
			if(!roadList.contains(newRoadList)) {
				roadList.addAll(newRoadList);
			}
		}
		System.out.println("Connected road points :");
		for(Integer road : roadList) {
			System.out.print("	"+ road);
		}
		System.out.println();
		System.out.println("Enter 1st check point ");
		int xPoint = sc.nextInt();
		//System.out.println("Enter 2nd check point ");
		int yPoint = sc.nextInt();
		if(roadList.contains(xPoint) && roadList.contains(yPoint)) {
			System.out.println("1");
		}else
			System.out.println("0");
		

	}

}
