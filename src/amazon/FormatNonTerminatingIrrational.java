package amazon;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class FormatNonTerminatingIrrational {
	
	/****
	 * Input numerator and denominator 
	 * e.g 6 & 2 completely divisible display 3
	 * 
	 *	7 & 3  2.3333 output 2.(3)
	 *	125 & 999 0.125125 output 0.(125)
	 ****/
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input numerator and denominator value: ");
		char input = 'Y';
		while(input == 'Y' || input == 'y') {
			int num = sc.nextInt();
			int den = sc.nextInt();
			
			/**Validation for numerator and denominator value*/
			if(!(num >= 1 && den >= 1 && den <= 100000)) {
				System.out.println("Numerator should be greater then 1 and Denominator in range from 1-100000");
				return;
			}
			
			int rem = num % den;
			if(rem == 0) {
				System.out.println(num/den);
			} else {
				int quo = num / den;

				Map<Integer, Integer> map = new LinkedHashMap<Integer, Integer>();
				while(rem != 0 && !map.containsKey(rem)) {					
					quo = (rem * 10) / den;
					map.put(rem, quo);
					rem = (rem *10) % den;					
				}
				
				if(rem == 0) {
					System.out.println((double)num/den);
				}
				else {
					int fractionValue = 0;
					for(Integer key : map.keySet()) {
						//System.out.println("key : "+ key + " val : "+ map.get(key) + ": ");
						fractionValue = fractionValue * 10 + map.get(key);
					}
					System.out.println(num/den + ".(" + (int)fractionValue + ")");
				}					
			}
			System.out.println("continue y / n ? ");
			input = sc.next().charAt(0);
		}
		sc.close();
	}

}
